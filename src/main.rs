#![allow(dead_code)]

use std::fmt;
use std::ops::Add;
use std::ops::Sub;

fn main() {
    println!("Hello, world!");
}

#[derive(Debug, PartialEq)]
enum Direction {
    North,
    East,
    South,
    West,
}

enum LeftRight {
    Left,
    Right,
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct Coord(i16);

#[derive(Debug, PartialEq)]
struct Position {
    x: Coord,
    y: Coord,
    direction: Direction,
}

impl fmt::Display for Position {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Use `self.number` to refer to each positional data point.
        write!(f, "{}:{}:{}", self.x.0, self.y.0, self.direction)
    }
}

impl fmt::Display for Direction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Use `self.number` to refer to each positional data point.
        match self {
            Direction::North => {
                write!(f, "N")
            }
            Direction::East => {
                write!(f, "E")
            }
            Direction::South => {
                write!(f, "S")
            }
            Direction::West => {
                write!(f, "W")
            }
        }
    }
}

impl Add for Coord {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self((self.0 + other.0) % 10)
    }
}

impl Sub for Coord {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self((self.0 - other.0 + 10) % 10)
    }
}

fn move_rover(current_position: Position) -> Position {
    match current_position.direction {
        Direction::North => Position {
            x: current_position.x,
            y: current_position.y + Coord(1),
            direction: current_position.direction,
        },
        Direction::East => Position {
            x: current_position.x + Coord(1),
            y: current_position.y,
            direction: current_position.direction,
        },
        Direction::South => Position {
            x: current_position.x,
            y: current_position.y - Coord(1),
            direction: current_position.direction,
        },
        Direction::West => Position {
            x: current_position.x - Coord(1),
            y: current_position.y,
            direction: current_position.direction,
        },
    }
}

fn rotate_right(current_direction: Direction) -> Direction {
    match current_direction {
        Direction::North => Direction::East,
        Direction::East => Direction::South,
        Direction::South => Direction::West,
        Direction::West => Direction::North,
    }
}

fn rotate_left(current_direction: Direction) -> Direction {
    match current_direction {
        Direction::North => Direction::West,
        Direction::East => Direction::North,
        Direction::South => Direction::East,
        Direction::West => Direction::South,
    }
}

fn rotate_rover(current_position: Position, left_right: LeftRight) -> Position {
    Position {
        x: current_position.x,
        y: current_position.y,
        direction: match left_right {
            LeftRight::Left => rotate_left(current_position.direction),
            LeftRight::Right => rotate_right(current_position.direction),
        },
    }
}

// fn parse_execute_single_action(action_char: char, current_position: Position) -> Result<Position, E> {
fn parse_execute_single_action(action_char: char, current_position: Position) -> Position {
    match action_char {
        'M' => move_rover(current_position),
        'L' => rotate_rover(current_position, LeftRight::Left),
        'R' => rotate_rover(current_position, LeftRight::Right),
        _ => {
            panic!("not implemented");
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        move_rover, parse_execute_single_action, rotate_left, rotate_right, Coord, Direction,
        Position,
    };

    #[test]
    fn test_parse_single_action_move() {
        let start_position: Position = Position {
            x: Coord(5),
            y: Coord(5),
            direction: Direction::North,
        };
        let expected_position = Position {
            x: Coord(5),
            y: Coord(6),
            direction: Direction::North,
        };
        assert_eq!(
            parse_execute_single_action('M', start_position),
            expected_position
        );
    }

    #[test]
    fn test_parse_single_action_left() {
        let start_position: Position = Position {
            x: Coord(5),
            y: Coord(5),
            direction: Direction::North,
        };
        let expected_position = Position {
            x: Coord(5),
            y: Coord(5),
            direction: Direction::West,
        };
        assert_eq!(
            parse_execute_single_action('L', start_position),
            expected_position
        );
    }

    #[test]
    fn test_parse_single_action_right() {
        let start_position: Position = Position {
            x: Coord(5),
            y: Coord(5),
            direction: Direction::North,
        };
        let expected_position = Position {
            x: Coord(5),
            y: Coord(5),
            direction: Direction::East,
        };
        assert_eq!(
            parse_execute_single_action('R', start_position),
            expected_position
        );
    }

    #[test]
    fn test_output_formated_postion() {
        let position = Position {
            x: Coord(2),
            y: Coord(3),
            direction: Direction::North,
        };

        assert_eq!(format!("{}", position), "2:3:N")
    }

    #[test]
    fn test_move_north() {
        let start_position: Position = Position {
            x: Coord(5),
            y: Coord(5),
            direction: Direction::North,
        };
        let expected_position = Position {
            x: Coord(5),
            y: Coord(6),
            direction: Direction::North,
        };
        assert_eq!(move_rover(start_position), expected_position);
    }

    #[test]
    fn test_move_north_wrapped() {
        let start_position: Position = Position {
            x: Coord(5),
            y: Coord(9),
            direction: Direction::North,
        };
        let expected_position = Position {
            x: Coord(5),
            y: Coord(0),
            direction: Direction::North,
        };
        assert_eq!(move_rover(start_position), expected_position);
    }

    #[test]
    fn test_move_east() {
        let start_position: Position = Position {
            x: Coord(5),
            y: Coord(5),
            direction: Direction::East,
        };
        let expected_position = Position {
            x: Coord(6),
            y: Coord(5),
            direction: Direction::East,
        };
        assert_eq!(move_rover(start_position), expected_position);
    }

    #[test]
    fn test_move_east_wrapped() {
        let start_position: Position = Position {
            x: Coord(9),
            y: Coord(5),
            direction: Direction::East,
        };
        let expected_position = Position {
            x: Coord(0),
            y: Coord(5),
            direction: Direction::East,
        };
        assert_eq!(move_rover(start_position), expected_position);
    }

    #[test]
    fn test_move_south() {
        let start_position: Position = Position {
            x: Coord(5),
            y: Coord(5),
            direction: Direction::South,
        };
        let expected_position = Position {
            x: Coord(5),
            y: Coord(4),
            direction: Direction::South,
        };
        assert_eq!(move_rover(start_position), expected_position);
    }

    #[test]
    fn test_move_south_wrapped() {
        let start_position: Position = Position {
            x: Coord(5),
            y: Coord(0),
            direction: Direction::South,
        };
        let expected_position = Position {
            x: Coord(5),
            y: Coord(9),
            direction: Direction::South,
        };
        assert_eq!(move_rover(start_position), expected_position);
    }

    #[test]
    fn test_move_west() {
        let start_position: Position = Position {
            x: Coord(5),
            y: Coord(5),
            direction: Direction::West,
        };
        let expected_position = Position {
            x: Coord(4),
            y: Coord(5),
            direction: Direction::West,
        };
        assert_eq!(move_rover(start_position), expected_position);
    }

    #[test]
    fn test_move_west_wrapped() {
        let start_position: Position = Position {
            x: Coord(0),
            y: Coord(5),
            direction: Direction::West,
        };
        let expected_position = Position {
            x: Coord(9),
            y: Coord(5),
            direction: Direction::West,
        };
        assert_eq!(move_rover(start_position), expected_position);
    }

    #[test]
    fn test_rotate_right_from_north() {
        assert_eq!(rotate_right(Direction::North), Direction::East);
    }

    #[test]
    fn test_rotate_right_from_east() {
        assert_eq!(rotate_right(Direction::East), Direction::South);
    }

    #[test]
    fn test_rotate_right_from_south() {
        assert_eq!(rotate_right(Direction::South), Direction::West);
    }

    #[test]
    fn test_rotate_right_from_west() {
        assert_eq!(rotate_right(Direction::West), Direction::North);
    }

    #[test]
    fn test_rotate_left_from_north() {
        assert_eq!(rotate_left(Direction::North), Direction::West);
    }

    #[test]
    fn test_rotate_left_from_east() {
        assert_eq!(rotate_left(Direction::East), Direction::North);
    }

    #[test]
    fn test_rotate_left_from_south() {
        assert_eq!(rotate_left(Direction::South), Direction::East);
    }

    #[test]
    fn test_rotate_left_from_west() {
        assert_eq!(rotate_left(Direction::West), Direction::South);
    }

    #[test]
    fn example1() {
        let input = "MMRMMLM";
        let output = "2:3:N";

        // let mut result = Position {
        //     x: Coord(0),
        //     y: Coord(0),
        //     direction: Direction::North
        // };
        // for char in input.chars() {
        //     pos = parse_execute_single_action(char, pos)
        // }

        let result = input.chars().fold(
            Position {
                x: Coord(0),
                y: Coord(0),
                direction: Direction::North,
            },
            |pos, char| parse_execute_single_action(char, pos),
        );

        assert_eq!(format!("{}", result), output);
    }

    /*#[test]
        fn example2() {
            let input= "MMMMMMMMMM";
            let output = "0:0:N";
        }
    */
}
